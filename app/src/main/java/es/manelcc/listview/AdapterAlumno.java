package es.manelcc.listview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by manelcc on 29/10/2015.
 */
public class AdapterAlumno extends BaseAdapter {

    private ArrayList<Alumno> items;
    Context ctx;

    public AdapterAlumno(Context ctx, ArrayList items) {
        this.items = items;
        this.ctx = ctx;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater)
                    ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            v = inflater.inflate(R.layout.itemlayout, parent, false);
        }

        Alumno alumno = (Alumno) getItem(position);

        ImageView img = (ImageView) v.findViewById(R.id.item_lv_img);
        img.setImageResource(alumno.getImg());

        TextView nombre = (TextView) v.findViewById(R.id.item_lv_nombre);
        nombre.setText(alumno.getNombre());

        TextView estadoCivil = (TextView) v.findViewById(R.id.item_lv_estadoCivil);
        estadoCivil.setText(alumno.getEstadoCivil());

        TextView genero = (TextView) v.findViewById(R.id.item_lv_genero);
        genero.setText(alumno.getGenero());

        TextView edad = (TextView) v.findViewById(R.id.item_lv_edad);
        edad.setText(String.format("%d", alumno.getEdad()));

        return v;
    }
}
