package es.manelcc.listview;

/**
 * Created by manelcc on 29/10/2015.
 */
public class Alumno {
    private int img;
    private String nombre;
    private int edad;
    private String genero;
    private String estadoCivil;

    public Alumno(int img, String nombre, int edad, String genero, String estadoCivil) {
        this.img = img;
        this.nombre = nombre;
        this.edad = edad;
        this.genero = genero;
        this.estadoCivil = estadoCivil;
    }

    public int getImg() {
        return img;
    }

    public void setImg(int img) {
        this.img = img;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getEstadoCivil() {
        return estadoCivil;
    }

    public void setEstadoCivil(String estadoCivil) {
        this.estadoCivil = estadoCivil;
    }
}
