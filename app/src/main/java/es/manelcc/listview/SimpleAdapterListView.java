package es.manelcc.listview;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

import java.util.ArrayList;

public class SimpleAdapterListView extends AppCompatActivity {

    ArrayList<Alumno> items;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_simple_adapter_list_view);

        //1 CONSTRUIR EL OBJETO A REPRESENTAR. Crear una clase Java que represente el Objeto.
        //2 GENERAR UN LISTADO DE OBJETOS. En esta caso puede ser Un ArrayList, un List o un Vector, HasMap, vaya
        //cualquier estructura de datos que estamos representando
        //3 LLENAR DE DATOS LA LISTA DE OBJETOS. Para ello hace falta
        //3.1 iniciar la variable de la lista.
        //3.2 instanciar a la clase objeto que queremos agregar a la lista. Clase Alumno
        //3.3 invocar al metodo .add para agregarlo a la lista.

        insertarAlumno();

        //4 Generar un adaptador
        //4.1 generar el xml que queremos mostrar en listView
        //4.2 El adaptador debera contener el contexto y los datos
        //4.3 Generar la clase adaptadora.
        //4.4 instanciar el adapter

        AdapterAlumno adapter = new AdapterAlumno(this, items);


        //5 Referenciar el listview
        ListView lista = (ListView) findViewById(R.id.listView);
        //6 setear el listView al adaptador
        lista.setAdapter(adapter);


    }

    private void insertarAlumno() {
        items = new ArrayList<>();

        Alumno antonio = new Alumno((R.mipmap.ic_launcher)
                , "antonio"
                , 25
                , "se le supone"
                , "casado");
        items.add(antonio);

        Alumno pepe = new Alumno((R.mipmap.ic_launcher)
                , "pepe"
                , 50
                , "se le supone"
                , "soltero");
        items.add(pepe);

        Alumno montse = new Alumno((R.mipmap.ic_launcher)
                , "montse"
                , 35
                , "femina"
                , "soltera");
        items.add(montse);

        Alumno javier = new Alumno((R.mipmap.ic_launcher)
                , "javier"
                , 38
                , "masculino"
                , "divorciado");
        items.add(javier);

        Alumno elena = new Alumno((R.mipmap.ic_launcher)
                , "Elena"
                , 38
                , "femenina"
                , "divorciado");
        items.add(elena);
    }
}
