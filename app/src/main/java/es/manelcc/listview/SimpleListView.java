package es.manelcc.listview;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class SimpleListView extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_simple_list_view);

        //Datos
        String[] version = new String[]{"lollipop", "honeycomb", "kitkat", "jelly bean", "donut"};

        //Referenciar el listView
        ListView lista = (ListView) findViewById(R.id.listView);

        //Instanciar el adapter

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                //contexto
                this,
                // layout generico
                android.R.layout.simple_list_item_1,
                //datos
                version);

        //setear el adapter al Listview
        lista.setAdapter(adapter);

        //interfaz click del listview

        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                parent.getItemAtPosition(position);
                Snackbar.make(view, "has pulsdo sobre  "
                                + parent.getItemAtPosition(position),
                        Snackbar.LENGTH_LONG).show();
            }
        });
    }
}
